package com.example.studycase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private EditText a;
    private EditText t;
    private TextView luas;
    private Button btnHitung;

    /**
     *method yang nantinya dipanggil ketika aplikasi berjalan
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /**
         * mencari atribut tertentu dan dibaca sebagai id tertentu
         */
        a = (EditText) findViewById(R.id.alas);
        t = (EditText) findViewById(R.id.tinggi);
        luas = (TextView) findViewById(R.id.hasil);
        btnHitung = (Button) findViewById(R.id.button);

    }

    /**
     *method onclick ketika button cek dijalankan
     */
    public void hitungLuas(View view) {
        String inputAlas, inputTinggi;

        inputAlas = a.getText().toString();
        inputTinggi = t.getText().toString();


        double A = Double.parseDouble(inputAlas);
        double T = Double.parseDouble(inputTinggi);

        /** rumus untuk mencari luas*/
        double hasil = A * T;

        /** command untuk menampilkan hasil perhitungan*/
        luas.setText(""+hasil);
    }
}
